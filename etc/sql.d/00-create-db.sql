CREATE EXTENSION dblink;
DO
$$
  BEGIN
    IF NOT EXISTS (SELECT FROM pg_database WHERE datname = 'explore') THEN
      PERFORM dblink_exec('dbname=' || current_database(),
                          'CREATE DATABASE explore');
    END IF;
  END
$$;
