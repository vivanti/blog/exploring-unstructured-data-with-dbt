\c explore

create schema if not exists scryfall;

create table if not exists
scryfall.landed_json_blobs (
  filename       text not null,
  sha1sum        varchar(40) not null,
  data           jsonb not null,
  loaded_at      timestamp without time zone not null
);
