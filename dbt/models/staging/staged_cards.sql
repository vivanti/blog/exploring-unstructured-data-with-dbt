{{
  config(
    materialized='table'
  )
}}

select
  card->>'id'             as print_id,
  card->>'oracle_id'      as oracle_id,
  card->>'name'           as name,
  card->>'type_line'      as type_line,
  (card->'cmc')::integer  as converted_mana_cost,
  card->>'rarity'         as rarity_code,

  card->'color_identity' ? 'w' as is_in_white,
  card->'color_identity' ? 'u' as is_in_blue,
  card->'color_identity' ? 'b' as is_in_black,
  card->'color_identity' ? 'r' as is_in_red,
  card->'color_identity' ? 'g' as is_in_green,

  -- legalities
  {% for format in var('known_formats') %}
    card->'legalities'->>'{{ format }}' = 'legal'
      as legal_in_{{ format }}
    {% if not loop.last %},{% endif %}
  {% endfor %}
from
  {{ ref('raw_cards') }}
