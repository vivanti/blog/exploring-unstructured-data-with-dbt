{{
  config(
    materialized='table'
  )
}}

with
partitioned as (
  select
    sha1sum,
    row_number() over(partition by sha1sum
                      order by loaded_at desc
                     ) as row_number
   from scryfall.landed_json_blobs
),

most_recent as (
  select sha1sum
  from partitioned
  where row_number = 1
)

select blobs.*
  from scryfall.landed_json_blobs blobs,
       most_recent
 where blobs.sha1sum = most_recent.sha1sum
