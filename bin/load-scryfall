#!/usr/bin/perl
use strict;
use warnings;
use utf8;
use open qw/:std :utf8/;

use Encode qw/encode_utf8/;
use Digest::SHA qw/sha1_hex/;
use DBI;
use DBD::Pg;

$ENV{LOAD_PG_DSN}
	or die "missing LOAD_PG_DSN environment variable!\n";
my $db = DBI->connect($ENV{LOAD_PG_DSN}, $ENV{LOAD_PG_USER}, $ENV{LOAD_PG_PASS})
	or die "unable to connect to '$ENV{LOAD_PG_DSN}' as '$ENV{LOAD_PG_USER}': @{[ DBI->errstr ]}\n";

$db->do('set search_path = scryfall');
my $st = $db->prepare('insert into landed_json_blobs (filename, sha1sum, data, loaded_at) values (?, ?, ?, now())')
	or die "unable to prepare insert query for 'landed_json_blobs' table: @{[ $db->errstr ]}\n";

# read file names off of stdin
while (<STDIN>) {
	chomp;
	next unless -f $_;
	my $filename = $_;

	open my $fh, "<", $filename;
	if (!$fh) {
		print STDERR "$filename: unable to open for reading: $!\n";
		next;
	}
	my $data = do { local $/; <$fh> };
	close $fh;

	my $sha1 = sha1_hex(utf8::is_utf8($data) ? encode_utf8($data) : $data);

	$st->execute($filename, $sha1, $data);
	print "loaded $_ [$sha1]\n";
}
